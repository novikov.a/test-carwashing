import 'bootstrap/dist/css/bootstrap.min.css'
import Header from './components/Header/Header.jsx'
import BreadcrumbDashboard from './components/BreadcrumbDashboard/BreadcrumbDashboard.jsx';
import Dashboard from './components/Dashboard/Dashboard.jsx'


function App() {
	return (
		<>
			<Header />
			<BreadcrumbDashboard />
			<Dashboard />
		</>
	)
}

export default App
