import Breadcrumb from 'react-bootstrap/Breadcrumb'
import './BreadcrumbDashboard.css'

function BreadcrumbDashboard() {
	return (
		<Breadcrumb>
			<Breadcrumb.Item href='#'>Главная</Breadcrumb.Item>
			<Breadcrumb.Item active>Дашборд</Breadcrumb.Item>
		</Breadcrumb>
	)
}

export default BreadcrumbDashboard