import Container from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'
import Navbar from 'react-bootstrap/Navbar'
import './Header.css'

function Header() {
	return (
		<Navbar expand='lg' className='bg-body-tertiary'>
			<Container>
				<Navbar.Brand href='#link'>
					<img src='/public/moyka.png' alt='logo' className='logoimg' />
				</Navbar.Brand>
				<Navbar.Toggle aria-controls='basic-navbar-nav' />
				<Navbar.Collapse id='basic-navbar-nav' className=''>
					<Nav className='ms-auto'>
						<Nav.Link href='#link'>Главная</Nav.Link>
						<Nav.Link href='#link'>Мониторинг</Nav.Link>
						<Nav.Link href='#link'>Финансы</Nav.Link>
						<Nav.Link href='#link'>Администратор</Nav.Link>
						<Nav.Link href='#link'>Планирование</Nav.Link>
						<Nav.Link href='#link'>Оборудование</Nav.Link>
						<Nav.Link href='#link'>Лояльность</Nav.Link>
					</Nav>
					<Nav className='ms-auto'>
						<Nav.Link href='#link'>Выход</Nav.Link>
					</Nav>
				</Navbar.Collapse>
			</Container>
		</Navbar>
	)
}

export default Header
