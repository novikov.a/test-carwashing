import TitleText from '../TitleText/TitleText'

function DashboardTask(props) {
	return (
		<div className='item '>
			<h3>{props.Name}</h3>
			<ul>
				<TitleText Title='Задача' Text='lorem lorem lorem' Id='1' />
				<TitleText Title='Календарь' Text='созвон созвонов созвонович' Id='2' />
				<TitleText Title='Что-то еще' Text='срочно оплатить счет' Id='3' />
			</ul>
		</div>
	)
}

export default DashboardTask