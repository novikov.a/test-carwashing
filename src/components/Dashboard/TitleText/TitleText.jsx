import "./TitleText.css"

function Dashboard(props) {
	return (
		<li>
			<div className='form-check'>
				<input
					className='form-check-input'
					type='checkbox'
					value=''
					id={props.Id}
					
				/>
				<label className='form-check-label' for={props.Id}>
					<b>{props.Title}</b> - {props.Text}
				</label>
			</div>
		</li>
	)
}

export default Dashboard
