import './Dashboard.css'
import DashboardTask from './DashboardTask/DashboardTask'
import DashboardMessege from './DashboardMessege/DashboardMessege'
import Carusel from './Carusel/Carusel'

function Dashboard() {
    return (
			<div>
				<container>
						<DashboardTask Name='Задачи' />
						<DashboardMessege Name='Сообщения' />
				</container>
				<Carusel />
			</div>
		)
}

export default Dashboard
