import TitleText from '../TitleText/TitleText'

function DashboardMessege(props) {
	return (
		<div className='item '>
			<h3>{props.Name}</h3>
			<ul>
				<TitleText
					Title='Иванов'
					Text='Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis, cumque delectus? Nemo voluptatibus ducimus laborum, vel a cumque ipsam molestiae vero, error alias expedita. Saepe soluta ad quisquam. Itaque, explicabo.'
					Id='4'
				/>
				<TitleText
					Title='Петров'
					Text='Как Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis, cumque delectus? Nemo voluptatibus ducimus laborum, vel a cumque ipsam molestiae vero, error alias expedita. Saepe soluta ad quisquam. Itaque, explicabo.'
					Id='5'
				/>
				<TitleText
					Title='Сидоров'
					Text='Что Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corporis, cumque delectus? Nemo voluptatibus ducimus laborum, vel a cumque ipsam molestiae vero, error alias expedita. Saepe soluta ad quisquam. Itaque, explicabo.'
					Id='6'

				/>
			</ul>
		</div>
	)
}

export default DashboardMessege
