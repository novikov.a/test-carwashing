//import Carousel from 'react-bootstrap/Carousel'
import './Carusel.css'

function Carusel() {
	return (
		<div className='carusel'>
			<div
				id='carouselExampleControls'
				className='carousel carousel-dark slide'
				data-bs-ride='carousel'
			>
				<div className='carousel-inner'>
					<div className='carousel-item active'>
						<img src='/public/img`.png' className='d-block w-100' alt='1' />
					</div>
					<div className='carousel-item'>
						<img src='/public/img2.png' className='d-block w-100' alt='2' />
					</div>
					<div className='carousel-item'>
						<img src='/public/img3.png' className='d-block w-100' alt='3' />
					</div>
				</div>
				<button
					className='carousel-control-prev'
					type='button'
					data-bs-target='#carouselExampleControls'
					data-bs-slide='prev'
				>
					<span
						className='carousel-control-prev-icon'
						aria-hidden='true'
					></span>
					<span className='visually-hidden'>Предыдущий</span>
				</button>
				<button
					className='carousel-control-next'
					type='button'
					data-bs-target='#carouselExampleControls'
					data-bs-slide='next'
				>
					<span
						className='carousel-control-next-icon'
						aria-hidden='true'
					></span>
					<span className='visually-hidden'>Следующий</span>
				</button>
			</div>
		</div>
	)
}

export default Carusel